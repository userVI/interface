# API reference

## API address

API server listens on **172.16.202.1:8999**.

## List of API resources:

### /api/v0/conn

- **GET:** Display list of  all connections available for the VM.

    *Example output: {"connections": [{"state": "online", "id": 0}, {"local_ip": "75.208.1.48", "primary_dns": "198.224.173.135", "state": "online", "interface": "ppp0", "secondary_dns":    "198.224.174.135", "id": 1, "remote_ip": "66.174.216.64"}, {"state": "failure", "id": 2}, {"state": "connecting", "id": 3}], "request_status": "success"}*

### /api/v0/conn/`<id>`

- **GET:** Display information about connection #`<id>`.

    Keys that always present:

    "id" - Connection number.

    "state" - Connection state. Can have values "online", "offline", “connecting”, “disconnecting”, “failure”. Connection has status “failure” after some number of unsuccessful connection attempts and stays in this status until a new connection attempt initialized.

    "active_for" - array of IP addresses of VMs using the connection.

    "proxy_address"/"proxy_port" - any request sent via this proxy will be forwarded via corresponding modem connection.

    Some other keys that could be seen: “local_ip”, “remote_ip”, “primary_dns”, “secondary_dns”, “interface”.

    Special connection with id 0 represents wired connection and always have status “online”.

    *Example output: {"state": "online", "request_status": "success", "id": 0}*

    *Example output: {"local_ip": "75.208.1.48", "primary_dns": "198.224.173.135", "request_status": "success", "state": "online", "interface": "ppp0", "secondary_dns": "198.224.174.135", "id": 1, "remote_ip": "66.174.216.64"}*

- **PATCH:** Manage connection #`<id>` state (online/offline).

    *Example request: {"state": "restart"}*

    “state” can be “on”, “online”, “off”, “offline”, “restart”

### /api/v0/conn/active

- **GET:** Display information about active connection.

- **PATCH:** Manage active connection state (online/offline/restart).

- **PUT:** Set another connection as active.

    *Example request: {“id”: 2}*

### /api/v0/modem

- **GET:** For internal usage.

- **POST:** For internal usage.

- **DELETE:** For internal usage.

## HEAD

Server always responds with just HTTP 200 OK to any HEAD request , useful for monitoring

## Error reporting

Server answers with HTTP 200 OK on successful request and with other codes on failures.
Additionally there should be “request_status” key present in each server reply with value either “success” on success or “error” on failure.
