from http.server import BaseHTTPRequestHandler, HTTPServer
import json, sys, os, threading, random




responsedict = {"request_status": "success"}
response = json.dumps(responsedict)
modemsListDict = {"request_status": "success", "modems": [{"kernel": "ttyACM6", "connection": {"local_ip": "75.208.235.208", "primary_dns": "198.224.173.135", "proxy_port": 9003, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp2", "secondary_dns": "198.224.174.135", "id": 3, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM6", "type": "serial"}, {"kernel": "ttyACM5", "connection": {"local_ip": "75.210.134.14", "primary_dns": "198.224.173.135", "proxy_port": 9005, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp4", "secondary_dns": "198.224.174.135", "id": 5, "remote_ip": "66.174.216.80"}, "devname": "/dev/ttyACM5", "type": "serial"}, {"kernel": "ttyACM2", "connection": {"local_ip": "75.210.162.27", "primary_dns": "198.224.174.135", "proxy_port": 9006, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp3", "secondary_dns": "198.224.173.135", "id": 6, "remote_ip": "66.174.217.64"}, "devname": "/dev/ttyACM2", "type": "serial"}, {"kernel": "ttyACM0", "connection": {"local_ip": "75.210.3.46", "primary_dns": "198.224.174.135", "proxy_port": 9007, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp6", "secondary_dns": "198.224.173.135", "id": 7, "remote_ip": "66.174.217.64"}, "devname": "/dev/ttyACM0", "type": "serial"}, {"kernel": "ttyACM4", "connection": {"local_ip": "75.210.90.78", "primary_dns": "198.224.174.135", "proxy_port": 9009, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp0", "secondary_dns": "198.224.173.135", "id": 9, "remote_ip": "66.174.217.80"}, "devname": "/dev/ttyACM4", "type": "serial"}, {"kernel": "ttyACM1", "connection": {"local_ip": "75.247.189.105", "primary_dns": "198.224.173.135", "proxy_port": 9011, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp5", "secondary_dns": "198.224.174.135", "id": 11, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM1", "type": "serial"}, {"kernel": "ttyACM3", "connection": {"local_ip": "75.208.98.217", "primary_dns": "198.224.174.135", "proxy_port": 9015, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp1", "secondary_dns": "198.224.173.135", "id": 15, "remote_ip": "66.174.217.80"}, "devname": "/dev/ttyACM3", "type": "serial"}]}

ip = '127.0.0.1'
port = 8999
class HttpProcessor(BaseHTTPRequestHandler):

    def _set_headers(self):
        if self.path == "/front/":
                self.send_response(200)
                self.send_header('Content-type', 'text/html; charset=utf-8')
                self.send_header("Access-Control-Allow-Origin", "*")
                self.send_header('Access-Control-Allow-Headers', 'content-type')
                self.send_header("Access-Control-Allow-Methods", "GET")
                self.send_header("Access-Control-Allow-Methods", "OPTIONS") 
                self.end_headers()
        else:        
                self.send_response(200)
                self.send_header("Access-Control-Allow-Origin", "*")
                self.send_header('Content-type', 'application/json; charset=utf-8')
                self.send_header('Access-Control-Allow-Headers', 'content-type')
                self.send_header("Access-Control-Allow-Methods", "POST")
                self.send_header("Access-Control-Allow-Methods", "GET")
                self.send_header("Access-Control-Allow-Methods", "OPTIONS") 
                self.send_header("Access-Control-Allow-Methods", "PATCH")
                self.send_header("Access-Control-Allow-Methods", "PUT")
                self.send_header("Access-Control-Allow-Methods", "DELETE")
                self.end_headers()
        return

    def do_GET(self):
        self._set_headers()
        if   self.path == "/api/v0/modem/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        elif self.path == "/front/":
             self.pagepath = sys.path[0]+'\index.html'
             f=open(os.path.abspath(self.pagepath), 'r', encoding='UTF-8')
             file=f.read()
             self.wfile.write(bytes(str(file), 'UTF-8'))
             f.close          
        elif self.path == "/api/v0/conn/0/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        elif self.path == "/api/v0/conn/1/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        elif self.path == "/api/v0/conn/2/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        elif self.path == "/api/v0/conn/3/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        elif self.path == "/api/v0/conn/4/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        elif self.path == "/api/v0/conn/5/":
             self.wfile.write(bytes(json.dumps(modemsListDict), 'UTF-8'))
        return

    def do_HEAD(self):
        self._set_headers()
        return
    
    def do_OPTIONS(self):
        self._set_headers()
        return

    def do_POST(self):
        self._set_headers()
        self.wfile.write(bytes(response, 'UTF-8'))
        return
    def do_PATCH(self):
        self._set_headers()
        if   self.path == "/api/v0/conn/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)
             print(str(self.jsonRequest))
        elif self.path == "/api/v0/conn/0/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)
             print(str(self.jsonRequest))
        elif self.path =="/api/v0/conn/1/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)
             print(str(self.jsonRequest))
        elif self.path =="/api/v0/conn/2/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)
             print(str(self.jsonRequest))
        elif self.path =="/api/v0/conn/3/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)
             print(str(self.jsonRequest)) 
        elif self.path =="/api/v0/conn/4/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)
             print(str(self.jsonRequest)) 
        elif self.path =="/api/v0/conn/5/":
             self.Request = self.rfile.read(int(self.headers.get('Content-Length')))
             self.jsonRequest = json.loads(self.Request)       
             print(str(self.jsonRequest))          
        self.wfile.write(bytes(response, 'UTF-8'))

    def do_DELETE(self):
        self._set_headers()
        self.wfile.write(bytes(response, 'UTF-8'))

    def do_PUT(self):
        self._set_headers()
        self.wfile.write(bytes(response, 'UTF-8'))
            
def ChangeResponse():
    global modemsListDict
    modemsListDict.update({"request_status": "success", "modems": [{"kernel": "fsdfsdf", "connection": {"local_ip": "75.208.235.208", "primary_dns": "198.224.173.135", "proxy_port": 9003, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp2", "secondary_dns": "198.224.174.135", "id": 33333, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM6", "type": "serial"}, {"kernel": "ttyACM6", "connection": {"local_ip": "75.208.235.208", "primary_dns": "198.224.173.135", "proxy_port": 9003, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp2", "secondary_dns": "198.224.174.135", "id": 3, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM6", "type": "serial"}, {"kernel": "ttyACM5", "connection": {"local_ip": "75.210.134.14", "primary_dns": "198.224.173.135", "proxy_port": 9005, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp4", "secondary_dns": "198.224.174.135", "id": 5, "remote_ip": "66.174.216.80"}, "devname": "/dev/ttyACM5", "type": "serial"}, {"kernel": "ttyACM2", "connection": {"local_ip": "75.210.162.27", "primary_dns": "198.224.174.135", "proxy_port": 9006, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp3", "secondary_dns": "198.224.173.135", "id": 6, "remote_ip": "66.174.217.64"}, "devname": "/dev/ttyACM2", "type": "serial"}, {"kernel": "ttyACM0", "connection": {"local_ip": "75.210.3.46", "primary_dns": "198.224.174.135", "proxy_port": 9007, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp6", "secondary_dns": "198.224.173.135", "id": 7, "remote_ip": "66.174.217.64"}, "devname": "/dev/ttyACM0", "type": "serial"}, {"kernel": "ttyACM4", "connection": {"local_ip": "75.210.90.78", "primary_dns": "198.224.174.135", "proxy_port": 9009, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp0", "secondary_dns": "198.224.173.135", "id": 9, "remote_ip": "66.174.217.80"}, "devname": "/dev/ttyACM4", "type": "serial"}, {"kernel": "ttyACM1", "connection": {"local_ip": "75.247.189.105", "primary_dns": "198.224.173.135", "proxy_port": 9011, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp5", "secondary_dns": "198.224.174.135", "id": 11, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM1", "type": "serial"}, {"kernel": "ttyACM3", "connection": {"local_ip": "75.208.98.217", "primary_dns": "198.224.174.135", "proxy_port": 9015, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp1", "secondary_dns": "198.224.173.135", "id": 15, "remote_ip": "66.174.217.80"}, "devname": "/dev/ttyACM3", "type": "serial"}]})

def ChangeResponse0():
    global modemsListDict
    modemsListDict.update({"request_status": "success", "modems": [{"kernel": "ttyACM6", "connection": {"local_ip": "75.208.235.208", "primary_dns": "198.224.173.135", "proxy_port": 9003, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp2", "secondary_dns": "198.224.174.135", "id": 3, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM6", "type": "serial"}, {"kernel": "ttyACM5", "connection": {"local_ip": "75.210.134.14", "primary_dns": "198.224.173.135", "proxy_port": 9005, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp4", "secondary_dns": "198.224.174.135", "id": 5, "remote_ip": "66.174.216.80"}, "devname": "/dev/ttyACM5", "type": "serial"}, {"kernel": "ttyACM2", "connection": {"local_ip": "75.210.162.27", "primary_dns": "198.224.174.135", "proxy_port": 9006, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp3", "secondary_dns": "198.224.173.135", "id": 6, "remote_ip": "66.174.217.64"}, "devname": "/dev/ttyACM2", "type": "serial"}, {"kernel": "ttyACM0", "connection": {"local_ip": "75.210.3.46", "primary_dns": "198.224.174.135", "proxy_port": 9007, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp6", "secondary_dns": "198.224.173.135", "id": 7, "remote_ip": "66.174.217.64"}, "devname": "/dev/ttyACM0", "type": "serial"}, {"kernel": "ttyACM4", "connection": {"local_ip": "75.210.90.78", "primary_dns": "198.224.174.135", "proxy_port": 9009, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp0", "secondary_dns": "198.224.173.135", "id": 9, "remote_ip": "66.174.217.80"}, "devname": "/dev/ttyACM4", "type": "serial"}, {"kernel": "ttyACM1", "connection": {"local_ip": "75.247.189.105", "primary_dns": "198.224.173.135", "proxy_port": 9011, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp5", "secondary_dns": "198.224.174.135", "id": 11, "remote_ip": "66.174.216.64"}, "devname": "/dev/ttyACM1", "type": "serial"}, {"kernel": "ttyACM3", "connection": {"local_ip": "75.208.98.217", "primary_dns": "198.224.174.135", "proxy_port": 9015, "state": "online", "proxy_address": "172.16.202.1", "interface": "ppp1", "secondary_dns": "198.224.173.135", "id": 15, "remote_ip": "66.174.217.80"}, "devname": "/dev/ttyACM3", "type": "serial"}]})


def run():
    server_address = (ip, port)
    server = HTTPServer(server_address, HttpProcessor)
    try:
        server.serve_forever()
    except Exception as e:
        wfile.write(bytes(str(e), 'UTF-8'))

t = threading.Timer(15, ChangeResponse)
t.start()
t = threading.Timer(40, ChangeResponse0)
t.start()
run()

