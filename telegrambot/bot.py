
import requests
import json
import re

class telegramBot:

    global token
    global url
    global offset
    global upd_id
    token = '305632566:AAEYOVOuzjYYRwGMWwYuCtwPVB41ROho4Co'
    url = 'https://api.telegram.org/bot' + token + '/'
    offset = 0
    upd_id = 0

    def __init__(self, offset, upd_id):
        self.token = token
        self.url = url
        self.offset = offset
        self.upd_id = upd_id

    @property
    def offset(self):
        return self.offset

    @offset.setter
    def setoffset(self, offs):
        self.offset = offs + 1
        return self.offset

    @property
    def upd_id(self):
        return self.upd_id

    @upd_id.setter
    def set_upd_id(self, offs):
        self.upd_id = offs
        return self.upd_id    
    
    def getUpdates (self):
        getup = url + 'getUpdates?timeout=30'
        if (self.offset == 0):
            urlup = str(getup)
            r = requests.get(urlup)
            urlup = r.json()
            if not urlup['result']:
                urlup = None
                return urlup
            return urlup
        else: 
            urlup = str(getup) + '&' + 'offset' + '=' + str(self.offset) 
            r = requests.get(urlup)
            urlup = r.json()
            if not urlup['result']:
                urlup = None
                return urlup
            return urlup

    def sendMessage(self, chat_id, text):
        send = url + 'sendMessage?chat_id=' + str(chat_id) + '&' + 'text=' + str(text) + '&&parse_mode=HTML'
        requests.get(send)

    def answerInlineQuery(self, data):
        global url
        print('ok, we in answer func')
        sendurl = url + 'answerInlineQuery'
        response = requests.post(sendurl, data=data)
        response.content
        print(response)
def main ():
    serverAddress = '172.16.202.1'
    serverPort = '8999'
    offset = 0
    upd_id = 0
    bot = telegramBot(offset, upd_id)
    
    while True:

# get json with data about user requests        
        url = bot.getUpdates()
        if (url != None) and not ('inline_query' in url['result'][0]):
            offs = url['result'][0]['update_id']
            bot.set_upd_id = int(offs)
            bot.setoffset = int(offs) 
            chat_id = url['result'][0]['message']['chat']['id']
            message_id = url['result'][0]['message']['message_id']
            textMessage = url['result'][0]['message']['text']
           
            # send response
            if textMessage == '/modems':
               r = requests.get('http://' + serverAddress + ':' + serverPort + '/api/v0/modem/')
               modemsDict = r.json()
               for modem in modemsDict['modems']:
                    text = '<b>Kernel:</b> '+modem['kernel']
                    bot.sendMessage(chat_id, text)
                    text = '<b>Devname:</b> '+modem['devname']
                    bot.sendMessage(chat_id, text)
                    text = '<b>Type:</b> '+modem['type']
                    bot.sendMessage(chat_id, text)
                    text = '---------------'    
                    bot.sendMessage(chat_id, text)
            elif textMessage == '/connections':   
                 r = requests.get('http://' + serverAddress + ':' + serverPort + '/api/v0/modem/')
                 modemsDict = r.json()
                 for modem in modemsDict['modems']:
                    if 'id' in modem['connection']:
                       text = '<b>ID:</b> '+modem['connection']['id']
                       bot.sendMessage(chat_id, str(text))
                    if 'state' in modem['connection']:
                       text = '<b>State:</b> '+modem['connection']['state']
                       bot.sendMessage(chat_id, text)
                    if 'local_ip' in modem['connection']:
                       text = '<b>Local IP:</b> '+modem['connection']['local_ip']
                       bot.sendMessage(chat_id, text)
                    if 'remote_ip' in modem['connection']:
                       text = '<b>Remote IP:</b> '+modem['connection']['remote_ip']
                       bot.sendMessage(chat_id, text)
                    if 'primary_dns' in modem['connection']:
                       text = '<b>Primary DNS:</b> '+modem['connection']['primary_dns']
                       bot.sendMessage(chat_id, text)
                    if 'secondary_dns' in modem['connection']:
                       text = '<b>Secondary DNS:</b> '+modem['connection']['secondary_dns']
                       bot.sendMessage(chat_id, text)
                    if 'interface' in modem['connection']:
                       text = '<b>Interface:</b> '+modem['connection']['interface']
                       bot.sendMessage(chat_id, text)
                    text = '---------------'    
                    bot.sendMessage(chat_id, text) 
            elif textMessage == '/modemscount':
                 r = requests.get('http://' + serverAddress + ':' + serverPort + '/api/v0/modem/')
                 modemsDict = r.json() 
                 message_text = str(len(modemsDict['modems']))
                 bot.sendMessage(chat_id, message_text)
            elif textMessage == '/restart':
                 message_text = 'Please, enter a connection ID in format: /n /restart_<id> /n example: restart_1'
                 bot.sendMessage(chat_id, message_text)
            elif textMessage == re.match('/restart_'):
                 restartid = re.split('_', textMessage)
                 requests.patch('http://' + serverAddress + ':' + serverPort + '/api/v0/conn/' + restartid[1], data = {'state': 'restart'})
                 message_text = 'Completed'
                 bot.sendMessage(chat_id, message_text)
                 
        elif (url != None) and ('inline_query' in url['result'][0]):
            urlid = url['result'][0]['inline_query']['id']
            str(urlid)
            query = url['result'][0]['inline_query']['query']
            if query == '/modemscount':  
                r = requests.get('http://' + serverAddress + ':' + serverPort + '/api/v0/modem/')
                modemsDict = r.json() 
                message_text = str(len(modemsDict['modems']))
                results = {'type': 'article', 'id': '1', 'title': 'count', 'input_message_content': {'message_text': message_text}}
                json.dumps(results)
                data = {'inline_query_id': urlid, 'results': results}
                bot.answerInlineQuery(data)
        else:
            continue
               
         
       
# передать запрос textMessage на наш сервер и в sendMessage ответить телеграм-серверу

if __name__ == '__main__':
    main()