from PyQt5 import QtCore, QtGui, QtWidgets, QtNetwork
from PyQt5.QtWidgets import QFrame
from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtWidgets import QWidget, QApplication, QActionGroup
from PyQt5.QtNetwork import QNetworkProxy
from functools import partial
from multiprocessing import Queue
import sys, os, requests, winshell, win32com.client, json, time, subprocess, winreg, resources
import win32com.shell.shell as shell
global serverAddress, port, PopupText, previous_id1, previous_id2, previous_state1, traydict, Systray, t, off_flag, reserv_id, local_ip, proxy_address, proxy_port, proxy_exist, server_fail, id_for_exclusion, m, n, is_fast_restart, Lock, quitdelay, not_restarted, not_restarted_activate, not_started, not_started1, not_started2, not_started3, not_started4

id_for_exclusion = 1010
previous_id1 = 0
previous_id2 = 0
previous_state1 = 0
PopupText = ''
local_ip = 'None'
off_flag = False
reserv_id = 1010
traydict = {}
proxy_address = 'None'
proxy_port = 'None'
proxy_exist = False
server_fail = False
id_for_exclusion = 'None'
hide_flag = True
m = 1
n = 0
is_fast_restart = False
Lock = False
quitdelay = False
not_restarted = False
not_restarted_activate = False
not_started = False
not_started1 = False
not_started2 = False
not_started3 = False
not_started4 = False

path_to_Notifier = r"C:\Notifier"

serverAddress = '192.168.0.159'
port = '8999'

ASADMIN = 'asadmin'

def set_proxy(proxy_address, proxy_port):
    proxy_server = 'https={}:{}'.format(proxy_address, proxy_port)
    try:
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Internet Settings', winreg.KEY_READ)
        subkey = winreg.QueryValueEx(key, 'ProxyEnable')
        if subkey[0] == 0:
            key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 0, winreg.KEY_SET_VALUE)
            winreg.SetValueEx(key, 'ProxyEnable', 0, winreg.REG_DWORD, 1)
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 0, winreg.KEY_SET_VALUE)
        winreg.SetValueEx(key, 'ProxyServer', 0, winreg.REG_SZ, proxy_server)
        winreg.SetValueEx(key, 'SecureProtocols', 0, winreg.REG_DWORD, aa)
        

        ssl2 = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r'SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Client', winreg.KEY_READ)
        subkey1 = winreg.QueryValueEx(ssl2, 'DisabledByDefault')
        if subkey1[0] == 1: 
            key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r'SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Client', 0, winreg.KEY_SET_VALUE)
            winreg.SetValueEx(key, 'DisabledByDefault', 0, winreg.REG_DWORD, 0)
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r'SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Client', 0, winreg.KEY_SET_VALUE)
        winreg.SetValueEx(key, 'Enabled', 0, winreg.REG_DWORD, 1)
        ssl3 = winreg.CreateKeyEx(winreg.HKEY_LOCAL_MACHINE, r'SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Client', 0, winreg.KEY_SET_VALUE)
        winreg.SetValueEx(ssl3, 'DisabledByDefault', 0, winreg.REG_DWORD, 0)
        winreg.SetValueEx(ssl3, 'Enabled', 0, winreg.REG_DWORD, 1)
       
    except Exception:
        pass
    reboot()


def reboot():
    os.startfile(path_to_Notifier + r"\ie.vbs")
    

def set_startup():
    
    try:
        
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Run', 0, winreg.KEY_SET_VALUE)
        winreg.SetValueEx(key, 'Notifier', 0, winreg.REG_SZ, path_to_Notifier + r"\Notifier_launcher.cmd")
    except Exception:
        pass
    reboot()

class CommunicateMove(QObject):
    moveevent = pyqtSignal()       

class Communicate(QObject):
    hideevent = pyqtSignal()   
               

class Timer(QtCore.QTimer):
    
    def __init__(self, parent=None):
        super().__init__()  
        self.timeout.connect(self.CheckConnections)
        self.started = True
        self.started1 = True
        self.started2 = True
        self.started3 = True
        self.started4 = True

    
    def startTimeHandler(self):
        global t, previous_id2, reserv_id, local_ip, not_started

        if self.started == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = '<b style="color:red">API server is not available</b>'
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_started = True
                self.starteventloop.exit()
                previous_id2 = int(reserv_id)

    def startTimeHandler1(self):
        global t, previous_id2, reserv_id, local_ip, not_started1z

        if self.started1 == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = '<b style="color:red">API server is not available</b>'
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_started1 = True
                self.starteventloop1.exit()
                previous_id2 = int(reserv_id)

    def startTimeHandler2(self):
        global t, previous_id2, reserv_id, local_ip, not_started2

        if self.started2 == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = '<b style="color:red">API server is not available</b>'
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_started2 = True
                self.starteventloop2.exit()
                previous_id2 = int(reserv_id)

    def startTimeHandler3(self):
        global t, previous_id2, reserv_id, local_ip, not_started3

        if self.started3 == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = '<b style="color:red">API server is not available</b>'
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_started3 = True
                self.starteventloop3.exit()
                previous_id2 = int(reserv_id)

    def startTimeHandler4(self):
        global t, previous_id2, reserv_id, local_ip, not_started4

        if self.started4 == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = '<b style="color:red">API server is not available</b>'
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_started4 = True
                self.starteventloop4.exit()
                previous_id2 = int(reserv_id)

    def GetActive0(self):
        global server_fail
        self.GetActive()
        while server_fail == True:
            self.GetActive()
           
   
    
    def GetActive(self): 
        global previous_state1, previous_id2, serverAddress, port, Systray, off_flag, reserv_id, local_ip, proxy_address, proxy_port, proxy_exist, server_fail, not_started, not_started1, not_started2, not_started3, not_started4
        self.not_restarted0 = False

    
        self.url = QtCore.QUrl("http://"+ serverAddress + ":" + port + "/api/v0/conn")
        

        self.request = QtNetwork.QNetworkRequest()
        self.request.setUrl(self.url)
        
        self.RequestManager = QtNetwork.QNetworkAccessManager()
        if proxy_exist == True:
            self.app_proxy = QNetworkProxy.applicationProxy()
            self.app_proxy.setHostName(proxy_address)
            
            self.proxy_port_int = int(proxy_port)
            self.app_proxy.setPort(self.proxy_port_int)
            self.app_proxy.setType(0)
            QNetworkProxy.setApplicationProxy(self.app_proxy)
            self.RequestManager.setProxy(self.app_proxy)
        else:
            proxy_address = '192.168.0.159'
            proxy_port = 9000
            self.app_proxy = QNetworkProxy.applicationProxy()
            self.app_proxy.setHostName(proxy_address)
 
            self.app_proxy.setPort(proxy_port)
            self.app_proxy.setType(0)
            QNetworkProxy.setApplicationProxy(self.app_proxy)
            self.RequestManager.setProxy(self.app_proxy)
            
        self.jsonElementss = self.RequestManager.get(self.request)
        
        self.started = False
        self.start_time = QtCore.QTimer()
        self.start_time.start(4000)
        self.start_time.timeout.connect(self.startTimeHandler)
        self.starteventloop = QtCore.QEventLoop()
        self.jsonElementss.finished.connect(self.starteventloop.exit)
        self.starteventloop.exec()
        self.started = True
        
        if not_started == False:
            off_flag = False
            
            self.jsonElementss = self.jsonElementss.readAll().data().decode('UTF-8')
            self.jsonElementss = str(self.jsonElementss)
            
            self.jsonElementss = json.loads(self.jsonElementss)
            self.activeforDict = {}

            if self.jsonElementss['request_status'] == 'success':
            
                self.c = Communicate()
                self.c.hideevent.connect(Systray.hide)

                self.idscount = 0
                self.active_exist = False
                self.select_least = False
                for ids in self.jsonElementss['connections']:
                    if 'id' in ids:
                        self.idscount += 1       
                if (self.idscount > 1): 
                    self.jsonElementsfor2loop = self.jsonElementss
                    for i in self.jsonElementss['connections']:
                        if (i['id'] != 0):
                            if (i['state'] != 'failure'):

                                if not 'active_for' in i:

                                    if (self.active_exist == False):

                                        self.count = 0
                                       
                                       
                                        if (i['state'] == 'offline'):
                                            while True:    
                                                self.sendurl = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/" + str(i['id']))
                                                
                                                self.rdata = {'state': 'online'}
                                                self.rdata = json.dumps(self.rdata)
                                                
                                                
                                                self.request = QtNetwork.QNetworkRequest()
                                                
                                                self.request.setUrl(self.sendurl)
                                                self.request.setRawHeader(QtCore.QByteArray(bytes('Accept', 'UTF-8')), QtCore.QByteArray(bytes('*/*', 'UTF-8')))
                                                self.request.setHeader(0, 'application/json')
                                                self.manager = QtNetwork.QNetworkAccessManager()
                                                
                                                self.app_proxy = QNetworkProxy.applicationProxy()
                                                self.app_proxy.setHostName(proxy_address)
                                                self.proxy_port_int = int(proxy_port)
                                                self.app_proxy.setPort(self.proxy_port_int)
                                                self.app_proxy.setType(0)
                                                QNetworkProxy.setApplicationProxy(self.app_proxy)
                                                self.manager.setProxy(self.app_proxy)
                                               
                                                self.rdata = bytes(self.rdata, 'UTF-8')
                                                self.data = QtCore.QByteArray(self.rdata)
                                               
                                                self.buffer = QtCore.QBuffer(self.manager)
                                                
                                                self.buffer.open(QtCore.QBuffer.ReadWrite)
                                                self.buffer.writeData(self.data)
                                                self.buffer.seek(0)

                                                self.patchbytes = bytes('PATCH', 'UTF-8')
                                                self.patchverb = QtCore.QByteArray(self.patchbytes)
                                               
                                                self.reply = self.manager.sendCustomRequest(self.request, self.patchverb, self.buffer)

                                                self.started1 = False
                                                self.start_time1 = QtCore.QTimer()
                                                self.start_time1.start(4000)
                                                self.start_time1.timeout.connect(self.startTimeHandler1)
                                                self.starteventloop1 = QtCore.QEventLoop()
                                                self.reply.finished.connect(self.starteventloop1.exit)
                                                self.starteventloop1.exec()
                                                self.started1 = True
                                                
                                                if not_started1 == False:

                                                    self.urlcheck = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/" + str(i['id']))
                                                    
                                                    self.requestcheck = QtNetwork.QNetworkRequest()
                                                    self.requestcheck.setUrl(self.urlcheck)
                                                    self.RequestManagercheck = QtNetwork.QNetworkAccessManager()
                                                    
                                                    self.app_proxy = QNetworkProxy.applicationProxy()
                                                    self.app_proxy.setHostName(proxy_address)
                                                    self.proxy_port_int = int(proxy_port)
                                                    self.app_proxy.setPort(self.proxy_port_int)
                                                    self.app_proxy.setType(0)
                                                    QNetworkProxy.setApplicationProxy(self.app_proxy)
                                                    self.RequestManagercheck.setProxy(self.app_proxy)
                                                    
                                                    self.jsonElementscheck = self.RequestManagercheck.get(self.requestcheck)
                                                    
                                                    self.eventloop2 = QtCore.QEventLoop()
                                                    self.jsonElementscheck.finished.connect(self.eventloop2.exit)
                                                    self.eventloop2.exec()

                                                    self.jsonElementscheck = self.jsonElementscheck.readAll().data().decode("utf-8")
                                                    self.jsonElementscheck = str(self.jsonElementscheck)
                                                    
                                                    self.jsonElementscheck = json.loads(self.jsonElementscheck)

                                                    proxy_address = self.jsonElementscheck['proxy_address']
                                                    proxy_port = str(self.jsonElementscheck['proxy_port'])
                                                    

                                                    if (self.jsonElementscheck['state'] == 'offline'):
                                                        off_flag = True
                                                    else:
                                                        off_flag = False
                                                        break

                                                    self.count += 1
                                                    if (self.count == 3) and (off_flag == True): break

                                        if (off_flag == True):
                                            continue

                                        if proxy_address != 'None' and proxy_port != 'None':
                                            set_proxy(proxy_address, proxy_port)

                                        self.sendurl = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/active")
                                        
                                        self.rdata = {'id': i['id']}
                                        self.rdata = json.dumps(self.rdata)
                                        
                                        
                                        self.request = QtNetwork.QNetworkRequest()
                                        
                                        self.request.setUrl(self.sendurl)
                                        self.request.setRawHeader(QtCore.QByteArray(bytes('Accept', 'UTF-8')), QtCore.QByteArray(bytes('*/*', 'UTF-8')))
                                        self.request.setHeader(0, 'application/json')
                                        self.manager = QtNetwork.QNetworkAccessManager()
                                       
                                        self.app_proxy = QNetworkProxy.applicationProxy()
                                        self.app_proxy.setHostName(proxy_address)
                                        self.proxy_port_int = int(proxy_port)
                                        self.app_proxy.setPort(self.proxy_port_int)
                                        self.app_proxy.setType(0)
                                        QNetworkProxy.setApplicationProxy(self.app_proxy)
                                        self.manager.setProxy(self.app_proxy)
                                      
                                        self.rdata = bytes(self.rdata, 'UTF-8')
                                        self.data = QtCore.QByteArray(self.rdata)
                                        
                                        self.buffer = QtCore.QBuffer(self.manager)
                                        
                                        self.buffer.open(QtCore.QBuffer.ReadWrite)
                                        self.buffer.writeData(self.data)
                                        self.buffer.seek(0)

                                        self.patchbytes = bytes('PUT', 'UTF-8')
                                        self.patchverb = QtCore.QByteArray(self.patchbytes)
                                        
                                        self.reply = self.manager.sendCustomRequest(self.request, self.patchverb, self.buffer)

                                        self.started2 = False
                                        self.start_time2 = QtCore.QTimer()
                                        self.start_time2.start(4000)
                                        self.start_time2.timeout.connect(self.startTimeHandler1)
                                        self.starteventloop2 = QtCore.QEventLoop()
                                        self.reply.finished.connect(self.starteventloop2.exit)
                                        self.starteventloop2.exec()
                                        self.started2 = True
                                        
                                        if not_started2 == False:
                                      
                                            off_flag = False
                                            
                                            self.urlcheck = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/" + str(i['id']))
                                            
                                            self.requestcheck = QtNetwork.QNetworkRequest()
                                            self.requestcheck.setUrl(self.urlcheck)
                                            self.RequestManagercheck = QtNetwork.QNetworkAccessManager()
                                            
                                            self.app_proxy = QNetworkProxy.applicationProxy()
                                            self.app_proxy.setHostName(proxy_address)
                                            self.proxy_port_int = int(proxy_port)
                                            self.app_proxy.setPort(self.proxy_port_int)
                                            self.app_proxy.setType(0)
                                            QNetworkProxy.setApplicationProxy(self.app_proxy)
                                            self.RequestManagercheck.setProxy(self.app_proxy)
                                            
                                            self.jsonElementscheck = self.RequestManagercheck.get(self.requestcheck)
                                            
                                            self.eventloop_check_conn = QtCore.QEventLoop()
                                            self.jsonElementscheck.finished.connect(self.eventloop_check_conn.exit)
                                            self.eventloop_check_conn.exec()

                                            self.jsonElementscheck = self.jsonElementscheck.readAll().data().decode("utf-8")
                                            self.jsonElementscheck = str(self.jsonElementscheck)
                                            
                                            self.jsonElementscheck = json.loads(self.jsonElementscheck)
                                            
                                            # if modem has not been offline, it has local_ip, else it get local_ip in loop for offline modem
                                            if 'local_ip' in self.jsonElementscheck:
                                                local_ip = self.jsonElementscheck['local_ip']

                                            if ('proxy_address' in self.jsonElementscheck) and ('proxy_port' in self.jsonElementscheck):
                                                proxy_address = self.jsonElementscheck['proxy_address']
                                                proxy_port = str(self.jsonElementscheck['proxy_port'])

                                                set_proxy(proxy_address, proxy_port)
                                            else:
                                                continue

                                            proxy_exist = True
                                            
                                            # add submenu with action, associated with connection ID  
                                            self.id = self.jsonElementscheck['id']
                                            reserv_id = self.id
                                            previous_state1 = self.jsonElementscheck['state']
                                            self.connState = self.jsonElementscheck['state']

                                            self.traydict = {self.id: self.connState}
                                            Systray.setMenu(self.traydict)
                                            self.activeforDict.clear()
                                            self.active_exist = True
                                            self.select_least = False
                                            off_flag = False
                                            server_fail = False
                                   
                                else:
                                # if no connections without 'active_for' key, select connection with least count active_for IP's
                                    self.select_least = True
                                    self.activelist = i['active_for']
                                    self.activelist = list(self.activelist)
                                    self.leng = len(self.activelist)
                                    self.id = i['id']
                                    reserv_id = self.id
                                    self.activeforDict[id] = self.leng  

                    if (self.select_least == True): 
                        self.selected = min(self.activeforDict, key=self.activeforDict.get) 
                        self.id = self.selected
                        
                        for i in self.jsonElementsfor2loop['connections']:
                            if (i['id'] == self.id):
                               
                                if (i['state'] == 'offline'):

                                    while True:

                                        self.sendurl = QtCore.QUrl("http://"+ serverAddress + ":" + port + "/api/v0/conn/" + str(self.id))
                                        self.rdata = {'state': 'online'}
                                        self.rdata = json.dumps(self.rdata)
                                        
                                        self.request = QtNetwork.QNetworkRequest()
                                        self.manager3 = QtNetwork.QNetworkAccessManager()
                                    
                                        self.app_proxy = QNetworkProxy.applicationProxy()
                                        self.app_proxy.setHostName(proxy_address)
                                        self.proxy_port_int = int(proxy_port)
                                        self.app_proxy.setPort(self.proxy_port_int)
                                        self.app_proxy.setType(0)
                                        QNetworkProxy.setApplicationProxy(self.app_proxy)
                                        self.manager3.setProxy(self.app_proxy)
                                      

                                        self.request.setUrl(self.sendurl)
                                        self.request.setHeader(0, 'application/json')
                                        self.rdata = bytes(self.rdata, 'UTF-8')
                                        self.data = QtCore.QByteArray(self.rdata)
                                        self.buffer = QtCore.QBuffer()
                                        self.buffer.open(QtCore.QBuffer.ReadWrite)
                                        self.buffer.writeData(self.data)
                                        self.buffer.seek(0)
                                        self.patchbytes = bytes('PATCH', 'UTF-8')
                                        self.patchverb = QtCore.QByteArray(self.patchbytes)
                                        self.reply3 = self.manager3.sendCustomRequest(self.request, self.patchverb, self.buffer)

                                        self.started3 = False
                                        self.start_time3 = QtCore.QTimer()
                                        self.start_time3.start(4000)
                                        self.start_time3.timeout.connect(self.startTimeHandler1)
                                        self.starteventloop3 = QtCore.QEventLoop()
                                        self.reply.finished.connect(self.starteventloop3.exit)
                                        self.starteventloop3.exec()
                                        self.started3 = True

                                        if not_started3 == False:
                                        
                                            self.urlcheck = QtCore.QUrl("http://"+ serverAddress + ":" + port + "/api/v0/conn/" + str(i['id']))
                                            self.requestcheck = QtNetwork.QNetworkRequest()
                                            self.requestcheck.setUrl(self.urlcheck)
                                            self.RequestManagercheck = QtNetwork.QNetworkAccessManager()
                                            
                                            self.app_proxy = QNetworkProxy.applicationProxy()
                                            self.app_proxy.setHostName(proxy_address)
                                            self.proxy_port_int = int(proxy_port)
                                            self.app_proxy.setPort(self.proxy_port_int)
                                            self.app_proxy.setType(0)
                                            QNetworkProxy.setApplicationProxy(self.app_proxy)
                                            self.RequestManagercheck.setProxy(self.app_proxy)
                                            

                                            self.jsonElementscheck = self.RequestManagercheck.get(self.requestcheck)
                                            
                                            self.eventloop5 = QtCore.QEventLoop()
                                            self.jsonElementscheck.finished.connect(self.eventloop5.exit)
                                            self.eventloop5.exec()

                                            self.jsonElementscheck = self.jsonElementscheck.readAll().data().decode("utf-8")
                                            self.jsonElementscheck = str(self.jsonElementscheck)
                                            
                                            self.jsonElementscheck = json.loads(self.jsonElementscheck)

                                            if 'local_ip' in self.jsonElementscheck:
                                                local_ip = self.jsonElementscheck['local_ip']
                                            
                                            proxy_address = self.jsonElementscheck['proxy_address']
                                            proxy_port = str(self.jsonElementscheck['proxy_port'])  
                                            
                                            if (self.jsonElementscheck['state'] == 'offline'):
                                                off_flag = True
                                            else:
                                                off_flag = False
                                                break

                                            self.count += 1
                                            
                                            if (self.count == 3) and (off_flag == True): break 

                                if proxy_address != 'None' and proxy_port != 'None':
                                    set_proxy(proxy_address, proxy_port)

                                self.sendurl = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/active")
                                
                                self.rdata = {'id': i['id']}
                                self.rdata = json.dumps(self.rdata)                             
                                
                                self.request = QtNetwork.QNetworkRequest()
                                
                                self.request.setUrl(self.sendurl)
                                self.request.setRawHeader(QtCore.QByteArray(bytes('Accept', 'UTF-8')), QtCore.QByteArray(bytes('*/*', 'UTF-8')))
                                self.request.setHeader(0, 'application/json')
                                self.manager = QtNetwork.QNetworkAccessManager()
                               
                                self.app_proxy = QNetworkProxy.applicationProxy()
                                self.app_proxy.setHostName(proxy_address)
                                self.proxy_port_int = int(proxy_port)
                                self.app_proxy.setPort(self.proxy_port_int)
                                self.app_proxy.setType(0)
                                QNetworkProxy.setApplicationProxy(self.app_proxy)
                                self.manager.setProxy(self.app_proxy)
                              
                                self.rdata = bytes(self.rdata, 'UTF-8')
                                self.data = QtCore.QByteArray(self.rdata)
                                
                                self.buffer = QtCore.QBuffer(self.manager)
                                
                                self.buffer.open(QtCore.QBuffer.ReadWrite)
                                self.buffer.writeData(self.data)
                                self.buffer.seek(0)

                                self.patchbytes = bytes('PUT', 'UTF-8')
                                self.patchverb = QtCore.QByteArray(self.patchbytes)
                                
                                self.reply = self.manager.sendCustomRequest(self.request, self.patchverb, self.buffer)

                                proxy_exist = True
                                # add submenu with action, associated with connection ID 

                                self.url = QtCore.QUrl("http://"+ serverAddress + ":" + port + "/api/v0/conn/" + str(i['id']))
                                

                                self.request = QtNetwork.QNetworkRequest()
                                self.request.setUrl(self.url)
                                
                                self.RequestManager = QtNetwork.QNetworkAccessManager()
                                if proxy_exist == True:
                                    self.app_proxy = QNetworkProxy.applicationProxy()
                                    self.app_proxy.setHostName(proxy_address)
                                    
                                    self.proxy_port_int = int(proxy_port)
                                    self.app_proxy.setPort(self.proxy_port_int)
                                    self.app_proxy.setType(0)
                                    QNetworkProxy.setApplicationProxy(self.app_proxy)
                                    self.RequestManager.setProxy(self.app_proxy)
                                else:
                                    proxy_address = '192.168.0.159'
                                    proxy_port = 9000
                                    self.app_proxy = QNetworkProxy.applicationProxy()
                                    self.app_proxy.setHostName(proxy_address)
                                
                                    self.app_proxy.setPort(proxy_port)
                                    self.app_proxy.setType(0)
                                    QNetworkProxy.setApplicationProxy(self.app_proxy)
                                    self.RequestManager.setProxy(self.app_proxy)
                                    
                                self.jsonElementss = self.RequestManager.get(self.request)
                                
                                self.started4 = False
                                
                                self.start_time4 = QtCore.QTimer()
                                self.start_time4.start(4000)
                                self.start_time4.timeout.connect(self.startTimeHandler1)
                                self.starteventloop4 = QtCore.QEventLoop()
                                self.jsonElementss.finished.connect(self.starteventloop4.exit)
                                self.starteventloop4.exec()
                                self.started4 = True

                                if not_started4 == False:
                                
                                    off_flag = False
                                    
                                    self.jsonElementss = self.jsonElementss.readAll().data().decode('UTF-8')
                                    self.jsonElementss = str(self.jsonElementss)
                                    
                                    self.jsonElementss = json.loads(self.jsonElementss)


                                    # if modem has not been offline, it has local_ip, else it get local_ip in loop for offline modem
                                    if 'local_ip' in self.jsonElementss:
                                        local_ip = self.jsonElementss['local_ip']

                                    if ('proxy_address' in self.jsonElementss) and ('proxy_port' in self.jsonElementss):
                                        proxy_address = self.jsonElementss['proxy_address']
                                        proxy_port = str(self.jsonElementss['proxy_port'])

                                        set_proxy(proxy_address, proxy_port)
                                    else:
                                        continue

                                    self.id = str(self.id)
                                    reserv_id = self.id
                                    previous_state1 = i['state']
                                    self.connState = i['state']
                                    self.traydict = {self.id: self.connState}
                                    Systray.setMenu(self.traydict)
                                    server_fail = False

                else:
                    if server_fail == False:
                        if (previous_id2 != int(reserv_id)):
                            self.PopupText = '<b style="color:red">No available connections</b>'
                            self.popup = PopupWindowClass(self.PopupText)
                            self.popup.show()
                            previous_id2 != int(reserv_id)
                    server_fail = True
            else:
                if server_fail == False:
                    if (previous_id2 != int(reserv_id)):
                        self.PopupText = '<b style="color:red">Connection error (' + local_ip + ')</b>'
                        self.popup = PopupWindowClass(self.PopupText)
                        self.popup.show()
                        previous_id2 != int(reserv_id)
                server_fail = True
       
     

    def CheckConnections(self):
        global proxy_address, proxy_port, serverAddress, port, proxy_exist, reserv_id, Lock
        if Lock == False:
            self.c = Communicate()
            self.c.hideevent.connect(Systray.hide)
            
            self.url = QtCore.QUrl("http://"+ serverAddress + ":" + port + "/api/v0/conn/" + str(reserv_id))
            self.checkrequest = QtNetwork.QNetworkRequest()
            self.checkrequest.setUrl(self.url)
            self.checkRequestManager = QtNetwork.QNetworkAccessManager()
            if proxy_exist == True:
                self.app_proxy = QNetworkProxy.applicationProxy()
                self.app_proxy.setHostName(proxy_address)
                self.proxy_port_int = int(proxy_port)
                self.app_proxy.setPort(self.proxy_port_int)
                self.app_proxy.setType(0)
                QNetworkProxy.setApplicationProxy(self.app_proxy)
                self.checkRequestManager.setProxy(self.app_proxy)

            self.response = self.checkRequestManager.get(self.checkrequest)    
            
            self.onfin = partial(self.onFinished, self.response)
            self.response.finished.connect(self.onfin)
        else:
            pass

    def onFinished(self, response):
        global previous_id1, t, previous_id2, previous_state1, PopupText, Systray, traydict, t, local_ip, reserv_id, proxy_address, proxy_port
        if Lock == False:
           
            self.c = Communicate()
            self.c.hideevent.connect(Systray.hide)
           
            self.data = response.readAll().data().decode("utf-8")
            self.data = str(self.data)
            self.jsonElements = json.loads(self.data)

            # if connection not online, show notification
            if (self.jsonElements['state'] != previous_state1) or (('local_ip' in self.jsonElements) and (local_ip != self.jsonElements['local_ip'])) or (('proxy_address' in self.jsonElements) and (self.jsonElements['proxy_address'] != proxy_address)) or (('proxy_port' in self.jsonElements) and (self.jsonElements['proxy_port'] != int(proxy_port))):  
               
                self.c.hideevent.emit()
                
                self.id = self.jsonElements['id']
                reserv_id = self.id
                self.connState = self.jsonElements['state']
                
                
                self.previous_local_ip = local_ip
               
                    
                self.sendurln = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/activate")
                self.idnumber = self.jsonElements['id']
                self.rdata = {'id': self.idnumber}
                self.rdata = json.dumps(self.rdata)
                
                
                self.requestn = QtNetwork.QNetworkRequest()
                
                self.requestn.setUrl(self.sendurln)
                self.requestn.setRawHeader(QtCore.QByteArray(bytes('Accept', 'UTF-8')), QtCore.QByteArray(bytes('*/*', 'UTF-8')))
                self.requestn.setHeader(0, 'application/json')
                self.manager = QtNetwork.QNetworkAccessManager()
                if proxy_exist == True:
                    self.app_proxy = QNetworkProxy.applicationProxy()
                    self.app_proxy.setHostName(proxy_address)
                    self.proxy_port_int = int(proxy_port)
                    self.app_proxy.setPort(self.proxy_port_int)
                    self.app_proxy.setType(0)
                    QNetworkProxy.setApplicationProxy(self.app_proxy)
                    self.manager.setProxy(self.app_proxy)
                self.rdata = bytes(self.rdata, 'UTF-8')
                self.data = QtCore.QByteArray(self.rdata)
                
                self.buffer = QtCore.QBuffer(self.manager)
                
                self.buffer.open(QtCore.QBuffer.ReadWrite)
                self.buffer.writeData(self.data)
                self.buffer.seek(0)
              
                self.patchbytes = bytes('PUT', 'UTF-8')
                self.patchverb = QtCore.QByteArray(self.patchbytes)
                
                self.reply = self.manager.sendCustomRequest(self.requestn, self.patchverb, self.buffer)
                
                if 'local_ip' in self.jsonElements:
                    local_ip = self.jsonElements['local_ip']
                
                #if it's the same IP
                if ((self.jsonElements['state'] != previous_state1) and (('local_ip' in self.jsonElements) and (self.previous_local_ip == self.jsonElements['local_ip']))) or ((self.jsonElements['state'] != previous_state1) and (self.jsonElements['state'] == 'disconnecting') and (('local_ip' in self.jsonElements) and (self.previous_local_ip == self.jsonElements['local_ip'])) or not ('local_ip' in self.jsonElements)) or ((self.jsonElements['state'] != previous_state1) and (self.jsonElements['state'] == 'connecting') and (('local_ip' in self.jsonElements) and (self.previous_local_ip == self.jsonElements['local_ip'])) or not ('local_ip' in self.jsonElements)) or ((self.jsonElements['state'] != previous_state1) and (self.jsonElements['state'] == 'offline') and (('local_ip' in self.jsonElements) and (self.previous_local_ip == self.jsonElements['local_ip'])) or not ('local_ip' in self.jsonElements)) or ((self.jsonElements['state'] != previous_state1) and (self.jsonElements['state'] == 'online') and (('local_ip' in self.jsonElements) and (self.previous_local_ip == self.jsonElements['local_ip'])) or not ('local_ip' in self.jsonElements)):
                    self.PopupText = 'Connection ' + '<b style="font-family: Times New Roman, sans">' + local_ip + '</b>' + ' is ' + str(self.connState)
                    self.PopupText = 'Connection ' + '<b style="font-family: Times New Roman, sans">' + local_ip + '</b>' + ' is ' + str(self.connState)
                    self.popup = PopupWindowClass(self.PopupText)
                    self.popup.show()
                #if it's new IP
                else:
                    self.PopupText = 'Your new IP is ' + '<b style="font-family: Times New Roman, sans">' + local_ip + '</b>'
                    self.popup = PopupWindowClass(self.PopupText)
                    self.popup.show()


                self.id = str(self.id)
                self.traydict = {self.id: self.connState}
                
                Systray.setMenu(self.traydict)
                previous_state1 = self.jsonElements['state']
                
                proxy_address = self.jsonElements['proxy_address']
                self.port_int = self.jsonElements['proxy_port']
                proxy_port = str(self.port_int)
                
                set_proxy(proxy_address, proxy_port)

            
                
class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
   
    def __init__(self, parent=None):
        super().__init__()
        self.restarted_activate = True

    def setMenu(self, traydict):
        global local_ip, proxy_address, proxy_port, serverAddress, port, id_for_exclusion
              
        icn = QtGui.QIcon()
        icn.addFile(path_to_Notifier + r'\icon.ico', QtCore.QSize(24, 24))
        self.setIcon(icn)
        self.menu = QtWidgets.QMenu()
        self.a = QtWidgets.QAction()
        self.exit = QtWidgets.QAction()
        self.k = traydict.keys()
        self.v = traydict.values()
        self.k = list(self.k) 
        self.v = list(self.v) 
        self.id = self.k[0]
        self.id = str(self.id)
        id_for_exclusion = self.id
        self.sstate = self.v[0]
        self.sstate = str(self.sstate)
        self.exit.setText('Exit')
        self.a.setText('Restart connection ' + local_ip + ' — now is ' + self.sstate)
        
        
        self.sendurl = 'http://' + serverAddress + ':' + port + '/api/v0/conn/' + id_for_exclusion
        self.re = partial(self.Reset, self.sendurl)
    
        self.a.triggered.connect(self.re)  
        self.exit.triggered.connect(self.Quitfromclick)
        self.ActivationReason(QtWidgets.QSystemTrayIcon.DoubleClick)
        self.activated.connect(self.TrayEventHandler)
       
        self.menu.addAction(self.a)
        self.menu.addAction(self.exit)

        self.setContextMenu(self.menu)
        self.show()  
        

    def TrayEventHandler(self, reason):
        if reason == QtWidgets.QSystemTrayIcon.DoubleClick:
            self.Reset(self.sendurl)
       

   
    def Quitfromclick(self):
        global Systray, quitdelay

        Systray.hide()
        if quitdelay == True:
            self.eventloop012.exit()
        try:
            key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 0, winreg.KEY_READ)
            subkey = winreg.QueryValueEx(key, 'ProxyEnable')
            if subkey[0] == 1:
                key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 0, winreg.KEY_SET_VALUE)
                winreg.SetValueEx(key, 'ProxyEnable', 0, winreg.REG_DWORD, 0)
            key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Internet Settings', 0, winreg.KEY_SET_VALUE)
            winreg.DeleteValue(key, 'ProxyServer')
        except Exception:
            pass
        reboot()
        QtCore.QCoreApplication.quit()
    
    def RestartTimeHandler(self):
        global t, previous_id2, reserv_id, local_ip, not_restarted

        if self.restarted == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = 'Connection ' + '<b style="font-family: Times New Roman, sans">' + local_ip + "</b> can't be restarted"
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_restarted = True
                self.eventloop012.exit()
                previous_id2 = int(reserv_id)
             

    def RestartTimeHandler_activate(self):
        global t, previous_id2, reserv_id, local_ip, not_restarted_activate

        if self.restarted_activate == False:
            if (previous_id2 != int(reserv_id)):
                self.PopupText = "Can't to activate new connection"
                self.popup = PopupWindowClass(self.PopupText)
                self.popup.show()
                not_restarted_activate = True
                self.eventloop01.exit()
                previous_id2 = int(reserv_id)
                

    def Reset(self, sendurl):
        global proxy_address, t, proxy_port, proxy_exist, local_ip, serverAddress, port, previous_id1, previous_id2, previous_state1, PopupText, Systray, traydict, reserv_id, id_for_exclusion, is_fast_restart, quitdelay, Lock, not_restarted, not_restarted_activate
        Lock = True

        self.request_status_success = False
        self.not_restarted = False

        proxy_exist = False
        is_fast_restart = False

        if quitdelay == True:
            self.eventloop012.exit()

        self.c = Communicate()
        self.c.hideevent.connect(Systray.hide)
         
        
        self.url = QtCore.QUrl("http://"+ serverAddress + ":" + port + "/api/v0/conn")
        
        self.request = QtNetwork.QNetworkRequest()
        self.request.setUrl(self.url)
        
        self.RequestManager = QtNetwork.QNetworkAccessManager()
        if proxy_exist == True:
            self.app_proxy = QNetworkProxy.applicationProxy()
            self.app_proxy.setHostName(proxy_address)
            
            self.proxy_port_int = int(proxy_port)
            self.app_proxy.setPort(self.proxy_port_int)
            self.app_proxy.setType(0)
            QNetworkProxy.setApplicationProxy(self.app_proxy)
            self.RequestManager.setProxy(self.app_proxy)
            
        self.jsonElementss = self.RequestManager.get(self.request)
        
        self.reseteventloop = QtCore.QEventLoop()
        self.RequestManager.finished.connect(self.reseteventloop.exit)
        self.reseteventloop.exec()
      
        self.jsonElementss = self.jsonElementss.readAll().data().decode('UTF-8')
        self.jsonElementss = str(self.jsonElementss)
        
        self.jsonElementss = json.loads(self.jsonElementss)
       
        for i in self.jsonElementss['connections']:
            
            if proxy_exist == False:         
                if (i['id'] != int(id_for_exclusion)):
                    if (i['id'] != 0):
                        if not ('active_for' in i):
                            if (i['state'] == 'online'):  
                                if is_fast_restart == False:                                    
                                    
                                    self.sendurln = QtCore.QUrl("http://" + serverAddress + ":" + port + "/api/v0/conn/active")
                                    self.idnumber = i['id']
                                    self.rdata = {'id': self.idnumber}
                                    self.rdata = json.dumps(self.rdata)
                                    
                                    
                                    self.requestn = QtNetwork.QNetworkRequest()
                                    
                                    self.requestn.setUrl(self.sendurln)
                                    self.requestn.setRawHeader(QtCore.QByteArray(bytes('Accept', 'UTF-8')), QtCore.QByteArray(bytes('*/*', 'UTF-8')))
                                    self.requestn.setHeader(0, 'application/json')
                                    self.manager = QtNetwork.QNetworkAccessManager()
                                    if proxy_exist == True:
                                        self.app_proxy = QNetworkProxy.applicationProxy()
                                        self.app_proxy.setHostName(proxy_address)
                                        self.proxy_port_int = int(proxy_port)
                                        self.app_proxy.setPort(self.proxy_port_int)
                                        self.app_proxy.setType(0)
                                        QNetworkProxy.setApplicationProxy(self.app_proxy)
                                        self.manager.setProxy(self.app_proxy)
                                    self.rdata = bytes(self.rdata, 'UTF-8')
                                    self.data = QtCore.QByteArray(self.rdata)
                                    
                                    self.buffer = QtCore.QBuffer(self.manager)
                                    
                                    self.buffer.open(QtCore.QBuffer.ReadWrite)
                                    self.buffer.writeData(self.data)
                                    self.buffer.seek(0)

                                    self.patchbytes = bytes('PUT', 'UTF-8')
                                    self.patchverb = QtCore.QByteArray(self.patchbytes)
                                    
                                    self.reply = self.manager.sendCustomRequest(self.requestn, self.patchverb, self.buffer)
                                    
                                    quitdelay_activate = True
                                    
                                    self.restarted_activate = False
                                    self.restart_time_activate = QtCore.QTimer()
                                    self.restart_time_activate.start(4000)
                                    self.restart_time_activate.timeout.connect(self.RestartTimeHandler_activate)
                                    self.eventloop01 = QtCore.QEventLoop()
                                    self.reply.finished.connect(self.eventloop01.exit)
                                    self.eventloop01.exec()
                                    self.restarted_activate = True
                                    
                                    quitdelay_activate = False
                                    if not_restarted_activate == False:

                                        off_flag = False
                                        
                                        self.reply = self.reply.readAll().data().decode('UTF-8')
                                        self.reply = str(self.reply)
                                        
                                        self.reply = json.loads(self.reply)
                                        
                                        if self.reply['request_status'] == 'success':
                                            self.request_status_success = True

                                            self.ielements = i

                                            proxy_exist = True
                                            is_fast_restart = True
                                
        
        self.url = QtCore.QUrl('http://' + serverAddress + ':' + port + '/api/v0/conn/' + id_for_exclusion)
        self.request = QtNetwork.QNetworkRequest()
        self.request.setUrl(self.url)
        
        self.request.setHeader(0, 'application/json')
        self.managerz = QtNetwork.QNetworkAccessManager()
        if proxy_exist == True:
            self.app_proxy = QNetworkProxy.applicationProxy()
            self.app_proxy.setHostName(proxy_address)
            self.proxy_port_int = int(proxy_port)
            self.app_proxy.setPort(self.proxy_port_int)
            self.app_proxy.setType(0)
            QNetworkProxy.setApplicationProxy(self.app_proxy)
            self.managerz.setProxy(self.app_proxy)
        
        self.rdata = {'state': 'restart'}
        self.rdata = json.dumps(self.rdata)
        self.rdata = bytes(self.rdata, 'UTF-8')
        self.data = QtCore.QByteArray(self.rdata)
        self.buffer1 = QtCore.QBuffer()
        self.buffer1.open(QtCore.QBuffer.ReadWrite)
        self.buffer1.writeData(self.data)
        self.buffer1.seek(0)
        self.patchbytes = bytes('PATCH', 'UTF-8')
        self.patchverb = QtCore.QByteArray(self.patchbytes)
        self.replyz = self.managerz.sendCustomRequest(self.request, self.patchverb, self.buffer1)

        quitdelay = True
       
        self.restarted = False
        self.restart_time = QtCore.QTimer()
        self.restart_time.start(4000)
        self.restart_time.timeout.connect(self.RestartTimeHandler)
        self.eventloop012 = QtCore.QEventLoop()
        self.replyz.finished.connect(self.eventloop012.exit)
        self.eventloop012.exec()
        self.restarted = True
       
        off_flag = False
        quitdelay = False
      
        if not_restarted == False:
            self.replyz = self.replyz.readAll().data().decode('UTF-8')
            self.replyz = str(self.replyz)
            
            self.replyz = json.loads(self.replyz)

            if (self.replyz['request_status'] == 'success') and (is_fast_restart == False):

                reserv_id = int(id_for_exclusion)
                
              

        
        if is_fast_restart == True:

            if self.request_status_success == True:
           
                self.id = self.ielements['id']
                self.connState = self.ielements['state']
                
                reserv_id = self.ielements['id']

                self.id = str(self.id)
                

                proxy_address = self.ielements['proxy_address']
                proxy_port = str(self.ielements['proxy_port'])
                set_proxy(proxy_address, proxy_port)

                

        Lock = False

class PopupWindowClass(QtWidgets.QWidget):
    popuphidden = QtCore.pyqtSignal()
   
    
    def __init__(self, PopupText):
        self.PopupText = PopupText
        super().__init__()
        self.setWindowFlags(QtCore.Qt.SplashScreen | QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        self.setMinimumSize(QtCore.QSize(360, 100))
        self.setMaximumSize(QtCore.QSize(360, 100))
        self.animation = QtCore.QPropertyAnimation(self, b"windowOpacity", self)
        self.animation.finished.connect(self.hide)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.hideAnimation)
        self.setupUi()
        self.setPopupText(PopupText)
        self.setContentsMargins(5, 0, 0, 0)

        

    def setupUi(self):
        icn = QtGui.QIcon()
        icn.addFile('icon.ico', QtCore.QSize(24, 24))
        app.setWindowIcon(icn)
        self.verticalLayout = QtWidgets.QHBoxLayout(self)
        self.verticalLayout.setContentsMargins(5, 0, 0, 0)
        self.label = QtWidgets.QLabel(self)
        self.labelpic = QtWidgets.QLabel(self)

        self.labelpic = QtWidgets.QLabel(self)
        self.labelpic.setAlignment(QtCore.Qt.AlignCenter)
        
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.pixmap = QtGui.QPixmap(':/images/icon.jpg')
        self.labelpic.setPixmap(self.pixmap)
        self.verticalLayout.addWidget(self.labelpic, 0)
        self.verticalLayout.addWidget(self.label, 1)
        self.verticalLayout.addStretch()
        
       
        appearance = self.palette()
        appearance.setColor(QtGui.QPalette.Normal, QtGui.QPalette.Window, QtGui.QColor("#c1c4c7"))
        self.setPalette(appearance)

   
    def setPopupText(self, PopupText):
        self.label.setText(PopupText)
        self.label.adjustSize()

   
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.dragPosition = event.globalPos() - self.frameGeometry().topLeft()
            event.accept()
   
    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.dragPosition)
            event.accept()

    def paintEvent(self, e):

        self.frame = QtGui.QPainter(self)
        self.frame.setViewport(0, 0, 360, 100)
        
        self.color = QtGui.QColor()
        self.color.setNamedColor('#000000')
        self.pen = QtGui.QPen()
        self.pen.setColor(self.color)
        self.pen.setWidthF(5)
        self.frame.setPen(self.pen)
        
        self.frame.drawRect(0, 0, 359, 99)
        self.frame.end()
   
    def show(self):
        global m, n
        self.setWindowOpacity(0.0)
        self.animation.setDuration(400)
        self.animation.setStartValue(0.0)
        self.animation.setEndValue(1.0)
        QtWidgets.QWidget.show(self)
        
        self.animation.start()
        self.timer.start(5000)
        
        try:
         
            screen_size = (QtWidgets.QApplication.desktop().availableGeometry().width(), QtWidgets.QApplication.desktop().availableGeometry().height())
            win_size = (self.frameSize().width(), self.frameSize().height())
            x = screen_size[0] - win_size[0] - 10
            y = screen_size[1] - (win_size[1] * m) - 10 - n
            self.move(x, y)
            
                
        except Exception as e:
            print(e)

    def hideAnimation(self):
        global m, n
        m=1
        n=0
        self.timer.stop()
        self.animation.setDuration(200)
        self.animation.setStartValue(1.0)
        self.animation.setEndValue(0.0)
        self.animation.start()
        
    
   
    def hide(self):
        global m, n
        if self.windowOpacity() == 0:
            QtWidgets.QWidget.hide(self)
            self.popuphidden.emit()
        if m < 10:
            if self.timer.isActive(): 
                m += 1
                n += 8
        
    
    
if __name__ == '__main__':
    global Systray
    
    if sys.argv[-1] != ASADMIN:
        script = os.path.abspath(sys.argv[0])
        params = ' '.join([script] + sys.argv[1:] + [ASADMIN])
        shell.ShellExecuteEx(lpVerb='runas', lpFile=sys.executable, lpParameters=params)
        #kill not admin-permission version of application
        sys.exit()

    set_startup()
       
    app = QtWidgets.QApplication(sys.argv)

    popup = PopupWindowClass(PopupText='')
    Systray = SystemTrayIcon()

    t = Timer()
    t.GetActive0()

    t.start(500)

    sys.exit(app.exec_())