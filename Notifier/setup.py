import sys

from cx_Freeze import setup, Executable

build_exe_options = {
"include_msvcr": True   
}



setup(
    name = "Notifier",
    version = "0.1",
    description = "",
    icon = "icon.ico",
    options = {"build_exe": build_exe_options},
    executables = [Executable("Notifier.py", base = "Win32GUI")]
)